from src.main import plus,multiply,newfn

def test_one():
    assert plus(1,5)==6
    assert plus(1,-5)==-4
    assert multiply(5,4)==20
    assert multiply(-2,3)==-6
    assert multiply(2,0)==0

def test_two():
    assert newfn(2)==4
    assert newfn(3)==9